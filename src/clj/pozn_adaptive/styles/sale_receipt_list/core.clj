(ns pozn-adaptive.styles.sale-receipt-list.core
  (:require [pozn-adaptive.styles.sale-receipt-list.receipt-list :as receipt-list]))

(def sale-receipt-list
  (into [] (concat receipt-list/sale-receipts-wrapper-styles)))