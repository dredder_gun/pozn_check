(ns pozn-adaptive.styles.sale-receipt-list.receipt-list)

(def sale-receipts-wrapper-styles
  [[:.sale-receipts_flex
    {:display :flex
     :align-items :stretch
     :flex-flow "column nowrap"
     :flex "0 0 auto"
     :justify-content :flex-start}]
   [:.sale-receipts
    {}]
   [:.sale-receipts__paid
    {:background "#9BD8F4"}]
   [:.sale-receipts__sent
    {:background "#BBF376"}]
   [:.sale-receipts__list
    {:width "100%"}]])