(ns pozn-adaptive.styles.pay
  (:require [garden.units :as u :refer [px pt percent]]))

(def pay-styles
  [[:.pay
    {:width (percent 100)
     :align-content :center}]
   [:.calculator__mar-btm
    {:margin-bottom (px 15)}]
   [:.calculator__h3
    {:width (percent 100)
     :text-align :center}]
   [:.calculator__sum-pay
    {:width (percent 100)
     :text-align :center}]
   [:.calculator__number_margin
    {:margin (px 15)}]
   [:.calculator__pay-btn
    {}]
   [:.header__text_pay
    {:justify-content :flex-start}]
   [:.pay_ul
    {:width (percent 100)
     :margin-top (px 20)
     :margin "auto"}]])
