(ns pozn-adaptive.styles.core
  (:require [garden.def :refer [defstylesheet defstyles]]
            [pozn-adaptive.styles.common.core :as common]
            [pozn-adaptive.styles.tables.core :as tables]
            [pozn-adaptive.styles.receipt.core :as receipt]
            [pozn-adaptive.styles.groups :as groups]
            [pozn-adaptive.styles.common.menu :as menu]
            [pozn-adaptive.styles.dish.dish :as dish]
            [pozn-adaptive.styles.common.input-btn :as input-btn]
            [pozn-adaptive.styles.pay :as pay]
            [pozn-adaptive.styles.sale-receipt-list.core :as sale-receipt-list]))

(defstyles pozn-styles
           (into []
                 (concat common/common-styles
                         tables/tables-styles
                         receipt/receipt-styles
                         groups/menu-styles
                         menu/menu-styles
                         dish/dish
                         input-btn/input-btn-styles
                         pay/pay-styles
                         sale-receipt-list/sale-receipt-list)))

