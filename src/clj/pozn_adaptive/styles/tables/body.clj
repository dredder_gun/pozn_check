(ns pozn-adaptive.styles.tables.body
  (:require [garden.units :as u :refer [px pt percent]]))

(def body-styles
  [[:.tables
    {:display :flex
     :flex-flow "column nowrap"
     :flex "0 0 auto"
     :justify-content :center
     :align-items :stretch}]
   [:.modal-wrapper__text-line_active
    {:border-radius (px 3)
     :padding (px 3)
     :background "#337AB7"}]])