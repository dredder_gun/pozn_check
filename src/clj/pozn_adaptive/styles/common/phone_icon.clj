(ns pozn-adaptive.styles.common.phone-icon
  (:require [garden.units :as u :refer [px pt percent]]
            [garden.color :as color :refer [hsl rgb]]))

(def phone-icon-styles
  [[:.phone-ico
    {:display :flex
     :justify-content "center"
     :align-items "center"
     :width (px 20)
     :height (px 20)
     :background "red"
     :border-radius (percent 50)
     :color "white"}]])