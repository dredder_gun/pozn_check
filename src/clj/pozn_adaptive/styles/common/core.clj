(ns pozn-adaptive.styles.common.core
  (:require [pozn-adaptive.styles.common.fixed-buttons :as fixed-buttons]
            [pozn-adaptive.styles.common.header :as header]
            [pozn-adaptive.styles.common.modal-window :as modal-window]
            [pozn-adaptive.styles.common.sandwich-menu :as sandwich-menu]
            [pozn-adaptive.styles.common.slide-window :as slide-window]
            [pozn-adaptive.styles.common.phone-icon :as phone-icon]
            [pozn-adaptive.styles.common.pointer-events :as pointer-events]
            [pozn-adaptive.styles.common.hidden :as visibility]
            [pozn-adaptive.styles.common.flex :as flex]))

(def common-styles
  (into [] (concat fixed-buttons/fixed-buttons-styles
                   header/header-styles
                   modal-window/modal-window-styles
                   sandwich-menu/sandwich-menu-styles
                   slide-window/slide-window-styles
                   phone-icon/phone-icon-styles
                   pointer-events/pe
                   visibility/hide-styles
                   flex/flexbox-styles)))

