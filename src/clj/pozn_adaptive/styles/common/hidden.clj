(ns pozn-adaptive.styles.common.hidden)

(def hide-styles
  [[:.invisible
    {:visibility :hidden}]
   [:.display-none
    {:display :none}]])