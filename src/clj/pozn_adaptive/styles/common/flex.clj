(ns pozn-adaptive.styles.common.flex)

(def flexbox-styles
  [[:.row-flex
    {:display :flex}]
   [:.column-flex
    {:display :flex
     :align-items "stretch"
     :flex-flow "column nowrap"
     :flex "0 0 auto"
     :justify-content :flex-start}]])