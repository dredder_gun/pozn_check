(ns pozn-adaptive.styles.common.pointer-events)

(def pe
  [[:.pe-none
    {:pointer-events "none"}]
   [:.pe-auto
    {:pointer-events "auto"}]])