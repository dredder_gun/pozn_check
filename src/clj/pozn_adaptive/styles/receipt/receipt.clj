(ns pozn-adaptive.styles.receipt.receipt
  (:require [garden.units :as u :refer [px pt percent]]))

(def receipt-styles
  [[:.receipt
    {:position "absolute"
     :height (percent 100)
     :width (percent 100)}]
   [:.receipt__flex
    {:display :flex
     :align-items "stretch"
     :flex-flow "column nowrap"
     :flex "0 0 auto"
     :justify-content :flex-start}]
   [:.receipt__select-mod__invisible
    {:display "none"}]
   [:.receipt__select-mod
    {:z-index 20
     :position "fixed"
     :width (percent 100)
     :height (percent 100)
     :background-color "rgba(0, 0, 0, 0.8)"}]
   [:.guests-list
    {:width (percent 100)
     :max-height (percent 100)
     :overflow :scroll
     :-webkit-overflow-scrolling "touch"}]
   [:.swipe-btn
    {:overflow :hidden
     :height (px 50)
     :margin-bottom 0
     :position :relative}]
   [:.swipe-btn__dot
    {:height (px 15)
     :width (px 15)
     :background-color "#bbb"
     :border-radius (percent 50)
     :display "inline-block"
     :margin-left (px 10)}]
   [:.swipe-btn__delete
    {:position :absolute
     :top 0
     :right 0
     :display :flex
     :justify-content :flex-end
     :align-items :center
     :background-color "#c9302c"
     :width (percent 100)
     :height (percent 100)
     :border-radius (px 4)}]
   [:.swipe-btn__delete__del-btn
    {:font-size (px 30)
     :background-color "#c9302c"
     :margin-left (px 15)
     :margin-right (px 30)}]
   [:.swipe-btn__mail-btn
    {:font-size (px 30)
     :background-color "#c9302c"
     :margin-right (px 30)}]
   [:.swipe-btn__card-btn
    {:font-size (px 30)
     :background-color "#c9302c"
     :margin-right (px 96)}]
   [:.swipe-btn__button
    {:position :relative
     :width (percent 80)
     :height (percent 100)
     :font-size (px 20)}]
   [:.swipe-btn__button:focus
    {:background-color "#337ab7"}]
   [:.swipe-btn__button:active
    {:background-color "#1d4567"}]
   [:.swipe-btn__button:hover
    {:background-color "#337ab7"}]
   [:.swipe-btn__button__selected
    {:background-color "#1d4567 !important"}]
   [:.swipe-btn__left-part
    {:position "absolute"
     :height (percent 100)
     :width (percent 20)
     :top 0
     :left 0}]
   [:.swipe-btn__price
    {:position :relative
     :width (percent 20)
     :padding "18px 0"}]
   [:.guest__table
    {:font-size (px 20)
     :width (percent 100)
     :table-layout "fixed"}]
   [:.guest__row
    {:padding (px 8)
     :width (percent 100)
     :position "relative"
     :border "1px solid grey"}]
   [:.guest__table__row_selected-dish
    {:background-color "#dddcda"}]
   [:.guest__table__area
    {:position "absolute"
     :width (percent 50)
     :height (percent 100)
     :top 0}]
   [:.guest__table__area_left
    {:width (percent 20)}]
   [:.guest__table__area_right
    {:right 0
     :width (percent 20)}]
   [:.guest__table__dish-text ]
   [:.guest__table__li
    {:word-break "break-word"}]
   [:.guest__table__dish-text__name
    {:margin 0
     :overflow "hidden"
     :white-space "nowrap"}]
   [:.fixed-buttons
    {:z-index 40}]
   [:.fixed-buttons_selected-dishes
    {:left (px 150)}]
   [:.fixed-buttons__static
    {:position :static}]
   [:.fixed-buttons_add-guest
    {:left (px 10)}]
   [:.modal-wrapper__header
    {:text-align "center"}]
   [:.guest-select
    {:position :fixed
     :display :flex
     :flex-direction :column
     :align-items :center
     :width (px 200)
     :bottom (px 10)
     :left (percent 50)
     :margin-left (px -100)
     :z-index 30}]
   [:.guests-select-mod
    {:position :fixed
     :display :flex
     :align-items :center
     :justify-content :space-around
     :width (percent 100)
     :bottom (px 10)
     :z-index 30}]
   [:.guests-select-mod__btn
    {:position :static
     :background-color "white"
     :color "black"}]
   [:.fixed-buttons_more
    {}]
   [:.fixed-buttons_card
    {}]
   [:.guests-select-mod__sum
    {:position :absolute
     :top (px -50)
     :font-size (px 34)
     :color "white"}]
   [:.fixed-buttons_send
    {}]
   [:.fixed-buttons_close
    {:width (px 55)
     :height (px 55)}]])