(ns pozn-adaptive.sale-receipt-list.view
  (:require [re-frame.core :as re-frame]
            [re-com.core :as re-com]
            [pozn-adaptive.sale-receipt-list.events :as events]
            [pozn-adaptive.sale-receipt-list.subs :as subs]
            [pozn-adaptive.modal-windows.window :refer [sandwich-menu]]
            [pozn-adaptive.statistic.view :refer [statistic-view]]
            [pozn-adaptive.events :as common-events]
            [pozn-adaptive.header :as header]))

(defn receipt-list-modal
  []
  [:div.modal-wrapper__panel-item
   [:div.modal-wrapper__text-line
    {:on-click #(re-frame/dispatch [::common-events/modal {:show? true
                                                           :child [statistic-view]
                                                           :size :small}])} "Статистика"]
   [:div.modal-wrapper__text-line
    {:on-click #(re-frame/dispatch [::common-events/flush-store])} "Очистить чеки"]])

(defn sale-receipt-list
  []
  (let [sale-receipts (re-frame/subscribe [::subs/sale-receipts])
        receipt-status-css (fn [sale-receipt]
                             (cond
                               (and (:paid? sale-receipt) (:sent? sale-receipt)) "sale-receipts__paid"
                               (:paid? sale-receipt) "sale-receipts__paid"
                               (:sent? sale-receipt) "sale-receipts__sent"
                               :default nil))
        button-label (fn [sale-receipt]
                       (str (if (not= (nth (:table sale-receipt) 0) 0)
                              (str "Стол: " (nth (:table sale-receipt) 0) "." (nth (:table sale-receipt) 1))
                              "Нет стола") " "
                            "Чек " (:id sale-receipt) " "
                            "Сумма " (:sum_price sale-receipt)))]
    [:div.sale-receipts_flex
     [header/header
      {:modal receipt-list-modal
       :children [[:h2 "Заказы"]]}]
     (into [:div.list-group.sale-receipts__list.sale-receipts_flex]
           (for [sale-receipt @sale-receipts]
             ^{:key (:id sale-receipt)} [re-com/button
                                         :class (str "list-group-item table-button list-button " (receipt-status-css sale-receipt))
                                         :label [:div.row-flex
                                                 (button-label sale-receipt)
                                                 (when (:phone sale-receipt) [:div.phone-ico.phone-ico__left-gap
                                                                              [:i {:class (str "zmdi zmdi-hc-fw-rc zmdi-phone")}]])]
                                         :on-click #(re-frame/dispatch [::events/turn-on-old-receipt (:id sale-receipt)])]))

     [:button.fixed-buttons
      {:on-click #(re-frame/dispatch [::events/change-layout :tables])}
      [re-com/md-icon-button
       :md-icon-name "zmdi-plus"
       :size :larger]]]))
