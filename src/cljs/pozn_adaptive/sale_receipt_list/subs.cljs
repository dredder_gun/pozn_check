(ns pozn-adaptive.sale-receipt-list.subs
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
  ::sale-receipts-signal
  (fn [db _]
    (:sale-receipts db)))

(re-frame/reg-sub
  ::sale-receipts
  :<- [::sale-receipts-signal]
  (fn [sale-receipts-signal _]
    (vals sale-receipts-signal)))

