(ns pozn-adaptive.helpers)

(defn allocate-next-id
  [items-list]
  ((fnil inc 0) (last (keys items-list))))

(defn allocate-next-id-vec
  [vec]
  ((fnil inc 0) {:id (last vec)}))

(defn str-to-int
  [str-num str-fraction]
  (let [neg-num-hn (fn [n] (if (< n 0) 0 n))]
    (if (= str-fraction "")
      (.parseInt js/window (neg-num-hn str-num))
      (+ (.parseInt js/window (neg-num-hn str-num))
         (-> (.parseInt js/window (neg-num-hn str-fraction))
             (/ 100))))))

(defn remove-last-str
  "docstring"
  [s]
  (subs s
        0
        (- (count s) 1)))