(ns pozn-adaptive.header
  (:require [re-frame.core :as re-frame]
            [pozn-adaptive.modal-windows.window :refer [sandwich-menu]]
            [pozn-adaptive.events :as common-events]))

(defn header
  "Компонент, который абстрагирует компонент шапки всего приложения
  class - css класс, который добавляет свойства классу header
  modal - компонент, который появится в модальном окне сэндвича
  children - вектор. Компоненты внутри header
  "
  [{:keys [class modal children] :or {children []}}]
  (into [:div.header {:class (str "header " class)}]
        (conj children
              (when modal
                [sandwich-menu #(re-frame/dispatch [::common-events/modal {:show? true
                                                                           :child [modal]
                                                                           :size  :small}])]))))