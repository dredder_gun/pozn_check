(ns pozn-adaptive.pay.view
  (:require [re-frame.core :as re-frame]
            [re-com.core :as re-com]
            [pozn-adaptive.pay.events :as events]
            [pozn-adaptive.pay.subs :as subs]
            [pozn-adaptive.header :as header]
            [reagent.core :as reagent]))

(defn pay-calculator
  []
  (let [sum_price (re-frame/subscribe [::subs/sum_price])
        dot? (reagent/atom false)
        int-num (reagent/atom "")
        fract-num (reagent/atom "")
        reset-all #(do (reset! dot? false)
                       (reset! int-num "")
                       (reset! fract-num ""))
        enable-dot (fn []
                     (if (= @int-num "")
                       nil
                       (swap! dot? not)))
        concat-to-string (fn [s]
                           (cond
                             (and @dot? (> (count @fract-num) 1)) nil
                             @dot? (swap! fract-num str s)
                             :default (swap! int-num str s)))
        remove-last (fn [s]
                      (subs s
                            0
                            (- (count s) 1)))
        subtract-count (fn []
                         (cond
                           (and @dot?
                                (= (count @fract-num) 1)) (do (reset! dot? false)
                                                              (reset! fract-num ""))
                           @dot? (swap! fract-num remove-last)
                           :default (swap! int-num remove-last)))
        result-str (reagent/track! (fn [] (if @dot? (str @int-num "." @fract-num)
                                                    (str @int-num))))]
    (fn []
      [:div.calculator.calculator__mar-btm
       [:h3.calculator__h3 (str "Осталось: " @sum_price)]
       [:div.calculator__buttons
        [:button.calculator__number.calculator__number_margin
         {:on-click #(concat-to-string "1")} 1]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(concat-to-string "2")} 2]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(concat-to-string "3")} 3]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(concat-to-string "4")} 4]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(concat-to-string "5")} 5]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(concat-to-string "6")} 6]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(concat-to-string "7")} 7]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(concat-to-string "8")} 8]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(concat-to-string "9")} 9]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(subtract-count)}
         [:i {:class (str "zmdi zmdi-hc-fw-rc zmdi-arrow-left")}]]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(concat-to-string "0")} 0]
        [:button.calculator__number.calculator__number_margin
         {:on-click #(enable-dot)} "."]]
       [:p.calculator__sum-pay @result-str]
       [:div.calculator__bottom-btns
        [:button.calculator__pay-btn
         {:on-click #(do
                       (re-frame/dispatch [::events/pay :cash @int-num @fract-num])
                       (reset-all))}
         "Наличные"]
        [:button.calculator__pay-btn
         {:on-click #(do
                       (re-frame/dispatch [::events/pay :card @int-num @fract-num])
                       (reset-all))}
         "Безнал"]]])))

(defn pay-view
  []
  (let [processed-operations (re-frame/subscribe [::subs/processed-operations])
        sum-paid (re-frame/subscribe [::subs/sum-paid])]
    (fn []
      [:div.pay.column-flex
       [header/header
        {:children [[re-com/md-icon-button :md-icon-name "zmdi-chevron-left"
                     :size :larger
                     :class "header__back-button"
                     :on-click #(re-frame/dispatch [::events/change-layout :new-sale-receipt])]
                    [:h2.header__text.header__text_pay.header__part
                     "Оплата"]]}]
       [pay-calculator]
       (into [:ul.pay_ul]
             (for [operation @processed-operations]
               [:li
                [:span (str "Сумма " (:sum operation))]
                [:span (case (:method operation)
                         :cash (str " Наличными "
                                    (when (:delivery operation)
                                      (str "Сдача: " (:delivery operation))))
                         :card " Безналом"
                         nil)]]))
       [:h3.calculator__h3 "Оплачено: " @sum-paid]])))

