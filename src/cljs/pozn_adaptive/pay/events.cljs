(ns pozn-adaptive.pay.events
  (:require [re-frame.core :as re-frame]
            [cljs.reader :refer [read-string]]
            [cljs-time.core :as time]
            [cljs-time.coerce :as coerce]
            [pozn-adaptive.helpers :as helpers]
            [pozn-adaptive.events :as events]))

(re-frame/reg-event-db
  ::change-layout
  [re-frame/trim-v]
  (fn [db [layout_key]]
    (if (= layout_key :new-sale-receipt)
      (-> (assoc db :layout layout_key)
          (assoc :old-dish? false))
      (assoc db :layout layout_key))))

(re-frame/reg-event-db
  ::pay-receipt
  [events/sale-receipts-interceptors re-frame/trim-v]
  (fn [{:keys [active-items] :as db} _]
    (-> (update-in db [:sale-receipts (:active-receipt active-items)]
               (fn [active-receipt]
                 (-> (assoc active-receipt :paid? true)
                     (update :dates merge {:pay (coerce/to-long (time/time-now))}))))
        (assoc :layout :sale-receipts-layout))))

(re-frame/reg-event-db
  ::pay
  [events/sale-receipts-interceptors re-frame/trim-v]
  (fn [{:keys [active-items] :as db} [pay-method int fraction]]
    "Перед оплатой проверить не превышает ли введённая сумма суммы в чеке"
    ; todo интерфейс должен реагировать, если входные цифры превысили сумму
    (let [sum-price (get-in db [:sale-receipts (:active-receipt active-items) :sum_price])
          sum-paid (get-in db [:sale-receipts (:active-receipt active-items) :sum_paid])
          left-to-pay (- sum-price sum-paid)
          result-num (helpers/str-to-int int fraction)]
      (cond
        (= sum-price sum-paid) db
        (and (= int "")
             (= fraction "")
             (= :cash pay-method)) (-> (assoc-in db [:sale-receipts (:active-receipt active-items) :sum_paid]
                                                 sum-price)
                                       (update-in [:sale-receipts (:active-receipt active-items) :pay-history]
                                                  conj
                                                  {:method pay-method :sum (- sum-price result-num) :delivery (- result-num left-to-pay)}))
        (and (= int "") (= fraction "")) (-> (assoc-in db [:sale-receipts (:active-receipt active-items) :sum_paid]
                                                       sum-price)
                                             (update-in [:sale-receipts (:active-receipt active-items) :pay-history]
                                                        conj
                                                        {:method pay-method :sum (- sum-price sum-paid)}))
        (and (= pay-method :card)
             (< left-to-pay result-num)) (-> (assoc-in db [:sale-receipts (:active-receipt active-items) :sum_paid]
                                                       sum-price)
                                             (update-in [:sale-receipts (:active-receipt active-items) :pay-history]
                                                        conj
                                                        {:method pay-method :sum (- sum-price sum-paid)}))
        (and (= pay-method :cash)
             (< left-to-pay result-num)) (-> (assoc-in db [:sale-receipts (:active-receipt active-items) :sum_paid]
                                                       sum-price)
                                             (update-in [:sale-receipts (:active-receipt active-items) :pay-history]
                                                        conj
                                                        {:method pay-method :sum left-to-pay :delivery (- result-num left-to-pay)}))
        (<= result-num sum-price) (-> (update-in db [:sale-receipts (:active-receipt active-items) :sum_paid]
                                                 +
                                                 result-num)
                                      (update-in [:sale-receipts (:active-receipt active-items) :pay-history]
                                                 conj
                                                 {:method pay-method :sum result-num}))
        :default db))))