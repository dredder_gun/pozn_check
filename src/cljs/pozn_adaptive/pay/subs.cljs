(ns pozn-adaptive.pay.subs
  (:require [re-frame.core :as re-frame]))

(re-frame/reg-sub
  ::active-receipt
  (fn [{:keys [active-items] :as db} _]
    (get-in db [:sale-receipts (:active-receipt active-items)])))

(re-frame/reg-sub
  ::sum_price
  :<- [::active-receipt]
  (fn [active-receipt _]
    (:sum_price active-receipt)))

(re-frame/reg-sub
  ::sum_paid
  :<- [::active-receipt]
  (fn [active-receipt _]
    (:sum_paid active-receipt)))

(re-frame/reg-sub
  ::processed-operations
  :<- [::active-receipt]
  (fn [active-receipt _]
    (:pay-history active-receipt)))

(re-frame/reg-sub
  ::sum-paid
  :<- [::active-receipt]
  (fn [active-receipt]
    (:sum_paid active-receipt)))