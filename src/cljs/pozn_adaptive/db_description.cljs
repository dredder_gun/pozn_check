(ns pozn-adaptive.db-description)

; спецификация всей app-db

{:layout :sale-receipts-layout
 :table-areas [{:id 1 :label "Зона 1" :tables (into [] (range 1 14))}
               {:id 2 :label "Зона 2" :tables (into [] (range 1 6))}
               {:id 3 :label "Зона 3" :tables (into [] (range 1 8))}
               {:id 4 :label "Зона 4" :tables (into [] (range 1 3))}]
 :active-items {:active-table [1 1]
                :active-receipt 1
                :active-dish-in-receipt {:receipt-id 1
                                         :id 3817
                                         :guest-id 1
                                         :count 1
                                         :modificators {1 {:id 1 :message "Модификатор 1"}
                                                        2 {:id 2 :message "Модификатор 2"}}
                                         :selected? false
                                         :full_name " Блинчики Болоньезе "
                                         :short_name "Блинчики Болоньезе"
                                         :price 95
                                         :weight "180"
                                         :c_time "20"
                                         :serving_order4 "2"
                                         :serving_order3 "2"
                                         :fractional "Нет"
                                         :group "Хиты"
                                         :tags ""
                                         :description "Два блина, фаршированные говяжьим фаршем в томатном соусе"
                                         :ings "Блины: яйцо, мука, молоко, вода, соль, сахар, сода. Начинка: говяжий фарш, лук, томатный соус, соль, перец"}
                :active-guest 1
                :tables-view 0
                :active-group "Хиты"}
 :editing false
 :dishes-select-mod false
 :guests-select-mod false
 :guests-select {:sum 0}
 :sale-receipts {1 {:id     1
                    :guests {1 {:id     1
                                :dishes [{:receipt-id 1
                                          :id 3817
                                          :guest-id 1
                                          :count 1
                                          :modificators {1 {:id 1 :message "Модификатор 1"}
                                                         2 {:id 2 :message "Модификатор 2"}}
                                          :selected? false
                                          :full_name " Блинчики Болоньезе "
                                          :short_name "Блинчики Болоньезе"
                                          :price 95
                                          :weight "180"
                                          :c_time "20"
                                          :serving_order4 "2"
                                          :serving_order3 "2"
                                          :fractional "Нет"
                                          :group "Хиты"
                                          :tags ""
                                          :description "Два блина, фаршированные говяжьим фаршем в томатном соусе"
                                          :ings "Блины: яйцо, мука, молоко, вода, соль, сахар, сода. Начинка: говяжий фарш, лук, томатный соус, соль, перец"}
                                         {:receipt-id 2
                                          :id 3791
                                          :guest-id 1
                                          :count 3
                                          :modificators {1 {:id 1 :message "Модификатор 1"}
                                                         2 {:id 2 :message "Модификатор 2"}}
                                          :selected? false
                                          :full_name " Бефстроганов из печени"
                                          :short_name "Бефстроганов"
                                          :price 110
                                          :weight "180/30"
                                          :c_time "20"
                                          :serving_order4 "3"
                                          :serving_order3 "3"
                                          :fractional "Нет"
                                          :group "Горячее"
                                          :tags ""
                                          :description "Обжаренная говяжья печень с луком в сливочном соусе"
                                          :ings "печень говяжья, сливки, лук, соль, перец. Украшение: помидор, огурец, лист салата."}]
                                :paid? 0 ; 0 не оплачен, 1 не до конца оплаче, 2 гость оплачен
                                :paid-sum 0
                                :sum 425}
                             2 {:id 2
                                :dishes [{:receipt-id 1
                                          :id 4654
                                          :guest-id 2
                                          :count 2
                                          :modificators {1 {:id 1 :message "Модификатор 1"}
                                                         2 {:id 2 :message "Модификатор 2"}}
                                          :selected? false
                                          :full_name " Блинчики Болоньезе "
                                          :short_name "Блинчики Болоньезе"
                                          :price 95
                                          :weight "180"
                                          :c_time "20"
                                          :serving_order4 "2"
                                          :serving_order3 "2"
                                          :fractional "Нет"
                                          :group "Хиты"
                                          :tags ""
                                          :description "Два блина, фаршированные говяжьим фаршем в томатном соусе"
                                          :ings "Блины: яйцо, мука, молоко, вода, соль, сахар, сода. Начинка: говяжий фарш, лук, томатный соус, соль, перец"}]
                                :sum 190}}
                    :table  [2 3] ; номер зоны и стола
                    :paid?  false
                    :sent?  false
                    :dates  {:origin 154545423} ; в формате long
                    :pay-history [{:method :cash :sum 200 :delivery 300} ; сдача для кэша
                                  {:method :card :sum 300}]
                    :sum_price 615
                    :sum_paid 500}}
 :slide-template {:show? false
                  :template nil}
 :calculator {:int-num 0
              :fractal-num 0
              :dot? false
              :current-value 0
              :current-str ""}
 :pay-session {:sale-receipt 1 ; в каком чеке не до конца сделали оплату
               :guests [2 3] ; какие гости ждут своей оплаты
               :remain-sum 0} ; сколько денег осталось оплатить
 :modal-windows {:show? false
                 :child nil}
 :statistic {:sold-dishes-count nil
             :common-sum nil
             :card-sum nil
             :cash-sum nil
             :paid-receipt-count nil
             :unpaid-receipt-count nil}
 :dish-search-query ""
 :swipe-events {:start-touch-position nil
                :start-swipe-time nil
                :direction nil
                :swiped-to-end? false
                :duration false
                :temporary-swipe nil
                :not-active? false}
 :menu-items [{:id 1 :group "Хиты" :list [{:id 3817 :full_name " Блинчики Болоньезе " :short_name "Блинчики Болоньезе" :price 95 :weight "180" :c_time "20" :serving_order4 "2" :serving_order3 "2" :fractional "Нет" :group "Хиты" :tags "" :description "Два блина, фаршированные говяжьим фаршем в томатном соусе" :ings "Блины: яйцо, мука, молоко, вода, соль, сахар, сода. Начинка: говяжий фарш, лук, томатный соус, соль, перец" }
                                          {:id 3819 :full_name " Блинчики с ветчиной и сыром " :short_name " Блинчики с ветчиной и сыром " :price 95 :weight "180" :c_time "20" :serving_order4 "2" :serving_order3 "2" :fractional "Нет" :group "Хиты" :tags "" :description "Два блина, фаршированные  ветчиной и расплавленным сыром." :ings "Блины: яйцо, мука, молоко, вода, соль, сахар, сода. Начинка: ветчина,мягкий сыр." }]}

              {:id 2 :group "Горячее" :list [{:id 3791 :full_name " Бефстроганов из печени" :short_name "Бефстроганов" :price 110 :weight "180/30" :c_time "20" :serving_order4 "3" :serving_order3 "3" :fractional "Нет" :group "Горячее" :tags "" :description "Обжаренная говяжья печень с луком в сливочном соусе" :ings "печень говяжья, сливки, лук, соль, перец. Украшение: помидор, огурец, лист салата." }
                                             {:id 3834 :full_name " Бифштекс с яйцом " :short_name "Бифштекс" :price 150 :weight "140/40/30" :c_time "20" :serving_order4 "3" :serving_order3 "3" :fractional "Нет" :group "Горячее" :tags "" :description "Запечённая котлета из рубленной говядины с яичницей." :ings "Говядина,сало,лук,соль,перец,яйцо,укроп. Украшение: помидор, огурец, лист салата,укроп." }]}]
 :window-width (.-innerWidth js/window)}